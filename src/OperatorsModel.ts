import { AtomDatabaseOperator } from "./AtomDatabaseOperator";
import { LogicalDatabaseOperator } from "./LogicalDatabaseOperator";
import { DatabaseOperator } from "./DatabaseOperator";

export type OperatorsModel<TModel> =
  | AtomDatabaseOperator<TModel>
  | LogicalDatabaseOperator<TModel>
  | DatabaseOperator;
