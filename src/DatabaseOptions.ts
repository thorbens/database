/**
 * Common database options for establishing a connection.
 */
export interface DatabaseOptions {
  /**
   * The user name to use for authentication.
   */
  username?: string;
  /**
   * The password to use for authentication.
   */
  password?: string;
  /**
   * The database connection url.
   */
  url: string;
  /**
   * The database name.
   */
  databaseName: string;
}
