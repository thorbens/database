import { DatabaseIDProvider } from "./DatabaseIDProvider";

/**
 * Abstract for a database model.
 * Extend this class to define Database Model elements for which a repository can be retrieved.
 */
export abstract class DatabaseModel
  extends Object
  implements DatabaseIDProvider {
  public readonly id!: string;
}
