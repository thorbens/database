import { ReadOnlyDatabaseQuery } from "./ReadOnlyDatabaseQuery";

export type DatabaseAggregation = Record<string, number>;
export type DatabaseAggregationMap = Record<string, DatabaseAggregation>;

/**
 * Represents a database result.
 *
 * @typeparam T The model used for this result.
 */
export interface DatabaseResult<TSource, TResult = TSource> {
  /**
   * Returns the executed query.
   */
  getQuery(): ReadOnlyDatabaseQuery<TSource, TResult>;

  /**
   * Returns the results.
   */
  getResults(): TResult[];

  /**
   * Returns the first result. If the result is empty, returns `null`.
   */
  getFirstResult(): TResult | null;

  /**
   * Returns the size of the results.
   */
  getSize(): number;

  /**
   * Returns the total size of results.
   */
  getTotalSize(): Promise<number>;

  /**
   * Returns the requested aggregation for the executed query.
   */
  getAggregations(): Promise<DatabaseAggregationMap>;
}
