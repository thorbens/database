import { DatabaseOperator } from "./DatabaseOperator";
import { DatabaseOrderByDirection } from "./DatabaseOrderByDirection";
import { KeyModel } from "./KeyModel";

export type AggregationOptions<TModel> = {
  field: KeyModel<TModel>;
  conditions?: ReadonlyArray<DatabaseOperator>;
};

/**
 * An aggregation map where the key represents the aggregation name
 * in the result and the value the field to aggregate.
 */
export type AggregationFields<TModel> = Record<
  string,
  AggregationOptions<TModel>
>;

/**
 * Represents a read only database query.
 *
 * @typeparam T The model used for this query.
 */
export interface ReadOnlyDatabaseQuery<TSource, TResult = TSource> {
  /**
   * Returns the offset.
   * The default offset is 0.
   */
  getOffset(): number;

  /**
   * Returns the limit.
   * The default limit is 0.
   */
  getLimit(): number;

  /**
   * Returns the order by key.
   * If no key is set, returns null.
   */
  getOrderBy(): string | null;

  /**
   * Returns the order by direction.
   */
  getOrderByDirection(): DatabaseOrderByDirection;

  /**
   * Returns the list of fields which should be returned.
   */
  getFields(): ReadonlyArray<KeyModel<TResult>>;

  /**
   * Returns this query in an object representation.
   */
  toObject(): object;

  /**
   * Returns true if this query has conditions set.
   */
  hasConditions(): boolean;

  /**
   * Returns all conditions for this array.
   */
  getConditions(): ReadonlyArray<DatabaseOperator>;

  /**
   * Returns the list of fields which should be aggregated.
   */
  getAggregationFields(): AggregationFields<TSource>;
}
