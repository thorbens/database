import { DatabaseOperator } from "./DatabaseOperator";
import { DatabaseOrderByDirection } from "./DatabaseOrderByDirection";
import {
  AggregationFields,
  ReadOnlyDatabaseQuery,
} from "./ReadOnlyDatabaseQuery";
import { KeyModel } from "./KeyModel";

/**
 * Represents a database query.
 *
 * @typeparam T The model used for this query.
 */
export interface DatabaseQuery<TSource, TResult = TSource>
  extends ReadOnlyDatabaseQuery<TSource, TResult> {
  /**
   * Sets the offset.
   *
   * @param offset The offset to set.
   */
  setOffset(offset: number): void;

  /**
   * Sets the new limit. Limit must great equal than 0.
   *
   * @param limit The limit to set.
   */
  setLimit(limit: number): void;

  /**
   * Sets the new order by key.
   *
   * @param orderBy The order by key.
   */
  setOrderBy(orderBy: string | null): void;

  /**
   * Sets the new order by direction.
   *
   * @param orderByDirection The order by direction to set.
   */
  setOrderByDirection(orderByDirection: DatabaseOrderByDirection): void;

  /**
   * Sets the fields which should be returned.
   *
   * @param fields
   */
  setFields(fields: ReadonlyArray<KeyModel<TResult>>): void;

  /**
   * Adds a condition to this query.
   * The condition will be frozen, so no further modification is allowed.
   *
   * @param condition The condition to add.
   */
  addCondition(condition: DatabaseOperator): void;

  /**
   * Removes all existing conditions.
   */
  clearConditions(): void;

  /**
   * Sets the list of fields which should be aggregated.
   *
   * @param fields The fields to be aggregated.
   */
  setAggregationFields(fields: AggregationFields<TSource>): void;
}
