import { DatabaseIDProvider } from "./DatabaseIDProvider";
import { DatabaseModel } from "./DatabaseModel";
import { DatabaseQuery } from "./DatabaseQuery";
import { DatabaseResult } from "./DatabaseResult";
import { ReadOnlyDatabaseQuery } from "./ReadOnlyDatabaseQuery";

/**
 * Definition of a repository for crud operations for a specific data type.
 *
 * @typeparam T The model for this repository.
 */
export interface Repository<T extends DatabaseModel> {
  /**
   * Inserts an element.
   *
   * @param element The element to insert.
   */
  insertOne(element: Readonly<T>): Promise<void>;

  /**
   * Inserts multiple elements.
   *
   * @param elements The elements to insert.
   */
  insert(elements: ReadonlyArray<T>): Promise<void>;

  /**
   * If an element with the given id already exists it will be replaced.
   *
   * @param element The element to insert.
   */
  insertOrReplaceOne(element: Readonly<T>): Promise<void>;

  /**
   * Updates the given element (replaces it with the new element).
   * The element must exist in the database, otherwise this operation will fail.
   *
   * @param element The element to update.
   */
  replaceOne(element: Readonly<T>): Promise<void>;

  /**
   * Updates the field with the given data.
   *
   * @param id The id of the dataset to update.
   * @param field The field to update.
   * @param data The data to update.
   */
  updateOneField(id: string, field: keyof T, data: T[keyof T]): Promise<void>;

  /**
   * Updates only the given properties in the given element.
   *
   * @param data The data to update.
   */
  updateOne(data: Partial<T> & DatabaseIDProvider): Promise<void>;

  /**
   * Updates only the given properties in the given element.
   *
   * @param query
   * @param data The data to update.
   */
  update(query: ReadOnlyDatabaseQuery<T>, data: Partial<T>): Promise<void>;

  /**
   * Finds an element by the given id.
   *
   * @param id The id to look for.
   */
  findById(id: string): Promise<T | null>;

  /**
   * Finds elements which matches the given query.
   * Use createQuery to build a query.
   *
   * @param query The query to perform.
   */
  findByQuery<TResult = T>(
    query: ReadOnlyDatabaseQuery<T, TResult>
  ): Promise<DatabaseResult<T, TResult>>;

  /**
   * Find the first elements which matches the given query and returns it.
   * If no element matches, null is returned.
   *
   * @param query The query to perform.
   */
  findOneByQuery<TResult = T>(
    query: ReadOnlyDatabaseQuery<T, TResult>
  ): Promise<DatabaseResult<T, TResult>>;

  /**
   * Creates a new query for this repository.
   */
  createQuery<TResult = T>(): DatabaseQuery<T, TResult>;

  /**
   * Deletes the element with the given id.
   *
   * @param id The element to delete.
   */
  deleteById(id: string): Promise<void>;

  /**
   * Deletes all elements that match the given query.
   *
   * @param query The query to perform.
   */
  deleteByQuery(query: ReadOnlyDatabaseQuery<T>): Promise<void>;

  /**
   * Counts the number of elements which matches the given query.
   * Limit and offset parameters in the query are _not_ ignored.
   *
   * @param query The query to perform.
   */
  count(query: ReadOnlyDatabaseQuery<T>): Promise<number>;

  /**
   * Deletes this repository.
   * Repositories should be drop via {@link Database#dropRepository} to ensure
   * correct processing of mapping and internal caching.
   */
  drop(): Promise<void>;

  /**
   * Returns the name of this repository.
   */
  getName(): Promise<string>;
}
