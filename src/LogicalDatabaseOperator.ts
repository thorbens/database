import { DatabaseOperator } from "./DatabaseOperator";
import { OperatorsModel } from "./OperatorsModel";

/**
 * Represents a condition for a query where the key should match the given value.
 * To transform this condition to a suitable representation, use the toObject method.
 *
 */
export interface LogicalDatabaseOperator<
  TModel,
  TValue extends DatabaseOperator[] = DatabaseOperator[]
> extends DatabaseOperator<TValue> {
  /**
   * Adds a new condition to this operator.
   *
   * @param condition The condition to add.
   */
  addCondition(condition: OperatorsModel<TModel>): void;
}
