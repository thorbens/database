import {
  CommonDatabaseOperatorObject,
  isCommonDatabaseOperatorObject,
} from "./CommonDatabaseOperatorObject";
import { KeyModel } from "../KeyModel";

/**
 * Common Database Operator interface to create an class instance from.
 *
 * @typeparam TModel The model used for this operator object.
 * @typeparam TValue The value used for this operator object.
 */
export interface CommonAtomDatabaseOperatorObject<TModel>
  extends CommonDatabaseOperatorObject {
  /**
   * The key for this operator.
   */
  readonly key: KeyModel<TModel>;
}

/**
 * Checks if the given object provides all properties of CommonAtomDatabaseOperatorObject.
 *
 * @typeparam T The model used for the operator.
 * @param obj The object to check.
 */
export function isCommonAtomDatabaseOperatorObject<T>(
  obj: object
): obj is CommonAtomDatabaseOperatorObject<T> {
  return isCommonDatabaseOperatorObject(obj) && "key" in obj;
}
