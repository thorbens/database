import { AbstractSearchDatabaseOperator } from "../AbstractSearchDatabaseOperator";
import { SearchDatabaseOperator } from "../SearchDatabaseOperator";
import { SearchOptions } from "../SearchOptions";
import { CommonDatabaseOperatorType } from "./CommonDatabaseOperatorType";

/**
 * A common database operator.
 */
export class CommonSearchDatabaseOperator<TValue extends string = string>
  extends AbstractSearchDatabaseOperator<TValue>
  implements SearchDatabaseOperator<TValue> {
  /**
   * The type for this operator.
   */
  private readonly type: CommonDatabaseOperatorType;

  /**
   * Creates a new database operator with the given key and value.
   *
   * @param type The type of this operator.
   * @param searchTerm
   * @param options
   */
  public constructor(
    type: CommonDatabaseOperatorType,
    searchTerm: TValue,
    options?: SearchOptions
  ) {
    super(searchTerm, options);
    this.type = type;
  }

  public toObject(): object {
    return this.toCommonObject();
  }

  public getType(): CommonDatabaseOperatorType {
    return this.type;
  }
}
