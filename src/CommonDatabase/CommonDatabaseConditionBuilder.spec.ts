import { CommonDatabaseOperatorType } from "..";
import { CommonDatabaseConditionBuilder } from "./CommonDatabaseConditionBuilder";

interface TestModel {
  bar: string;
  foo: string;
}

describe("CommonDatabaseConditionBuilder", () => {
  it("should create the requested operator correctly", async () => {
    const conditionBuilder = new CommonDatabaseConditionBuilder<TestModel>();
    const value = "foo";
    const key = "bar";
    const type = CommonDatabaseOperatorType.EQUAL;
    const operator = conditionBuilder.createAtomOperatorByType(
      type,
      key,
      value
    );
    if (!operator) {
      throw new Error("creating should not return null");
    }

    expect(operator.getValue()).toEqual(value);
    expect(operator.getKey()).toEqual(key);
    expect(operator.getType()).toEqual(type);
  });

  it("should create an and operator", async () => {
    const conditionBuilder = new CommonDatabaseConditionBuilder<TestModel>();
    const firstCondition = conditionBuilder.createEqualOperator("foo", "eins");
    const secondCondition = conditionBuilder.createEqualOperator("bar", "zwei");
    const operator = conditionBuilder.createAndOperator([
      firstCondition,
      secondCondition,
    ]);
    if (!operator) {
      throw new Error("creating should not return null");
    }

    expect(operator.getValue()).toEqual([firstCondition, secondCondition]);
    expect(operator.getType()).toEqual(CommonDatabaseOperatorType.AND);
  });

  it("should create an or operator", async () => {
    const conditionBuilder = new CommonDatabaseConditionBuilder<TestModel>();
    const firstCondition = conditionBuilder.createEqualOperator("bar", "eins");
    const secondCondition = conditionBuilder.createEqualOperator("foo", "zwei");
    const operator = conditionBuilder.createOrOperator([
      firstCondition,
      secondCondition,
    ]);
    if (!operator) {
      throw new Error("creating should not return null");
    }

    expect(operator.getValue()).toEqual([firstCondition, secondCondition]);
    expect(operator.getType()).toEqual(CommonDatabaseOperatorType.OR);
  });

  it("should create an equal operator", async () => {
    const conditionBuilder = new CommonDatabaseConditionBuilder<TestModel>();
    const value = "foo";
    const key = "bar";
    const operator = conditionBuilder.createEqualOperator(key, value);
    if (!operator) {
      throw new Error("creating should not return null");
    }

    expect(operator.getValue()).toEqual(value);
    expect(operator.getKey()).toEqual(key);
    expect(operator.getType()).toEqual(CommonDatabaseOperatorType.EQUAL);
  });

  it("should create a not operator", async () => {
    const conditionBuilder = new CommonDatabaseConditionBuilder<TestModel>();
    const value = [conditionBuilder.createEqualOperator("foo", "bar")];
    const operator = conditionBuilder.createNotOperator(value);
    if (!operator) {
      throw new Error("creating should not return null");
    }

    expect(operator.getValue()).toEqual(value);
    expect(operator.getType()).toEqual(CommonDatabaseOperatorType.NOT);
  });

  it("should create a not equal operator", async () => {
    const conditionBuilder = new CommonDatabaseConditionBuilder<TestModel>();
    const value = "foo";
    const key = "bar";
    const operator = conditionBuilder.createNotEqualOperator(key, value);
    if (!operator) {
      throw new Error("creating should not return null");
    }

    expect(operator.getValue()).toEqual(value);
    expect(operator.getKey()).toEqual(key);
    expect(operator.getType()).toEqual(CommonDatabaseOperatorType.NOT_EQUAL);
  });

  it("should create a reg exp operator", async () => {
    const conditionBuilder = new CommonDatabaseConditionBuilder<TestModel>();
    const value = /foo/i;
    const key = "bar";
    const operator = conditionBuilder.createRegExpOperator(key, value);
    if (!operator) {
      throw new Error("creating should not return null");
    }

    expect(operator.getValue().toString()).toEqual(value.toString());
    expect(operator.getKey()).toEqual(key);
    expect(operator.getType()).toEqual(CommonDatabaseOperatorType.REGEXP);
  });

  it("should create a less than operator", async () => {
    const conditionBuilder = new CommonDatabaseConditionBuilder<TestModel>();
    const value = "foo";
    const key = "bar";
    const operator = conditionBuilder.createLessThanOperator(key, value);
    if (!operator) {
      throw new Error("creating should not return null");
    }

    expect(operator.getValue()).toEqual(value);
    expect(operator.getKey()).toEqual(key);
    expect(operator.getType()).toEqual(CommonDatabaseOperatorType.LT);
  });

  it("should create a greater than operator", async () => {
    const conditionBuilder = new CommonDatabaseConditionBuilder<TestModel>();
    const value = "foo";
    const key = "bar";
    const operator = conditionBuilder.createGreaterThanOperator(key, value);
    if (!operator) {
      throw new Error("creating should not return null");
    }

    expect(operator.getValue()).toEqual(value);
    expect(operator.getKey()).toEqual(key);
    expect(operator.getType()).toEqual(CommonDatabaseOperatorType.GT);
  });

  it("should create a exists operator", async () => {
    const conditionBuilder = new CommonDatabaseConditionBuilder<TestModel>();
    const value = [conditionBuilder.createEqualOperator("foo", "foo")];
    const key = "bar";
    const operator = conditionBuilder.createExistsOperator(key, value);
    if (!operator) {
      throw new Error("creating should not return null");
    }

    expect(operator.getValue()).toEqual(value);
    expect(operator.getKey()).toEqual(key);
    expect(operator.getType()).toEqual(CommonDatabaseOperatorType.EXISTS);
  });

  it("should create a in operator", async () => {
    const conditionBuilder = new CommonDatabaseConditionBuilder<TestModel>();
    const value = ["foo"];
    const key = "bar";
    const operator = conditionBuilder.createInOperator(key, value);
    if (!operator) {
      throw new Error("creating should not return null");
    }

    expect(operator.getValue()).toEqual(value);
    expect(operator.getKey()).toEqual(key);
    expect(operator.getType()).toEqual(CommonDatabaseOperatorType.IN);
  });

  it("should create a match operator", async () => {
    const conditionBuilder = new CommonDatabaseConditionBuilder<TestModel>();
    const value = "foo";
    const key = "bar";
    const operator = conditionBuilder.createMatchSomeOperator(key, value);
    if (!operator) {
      throw new Error("creating should not return null");
    }

    expect(operator.getValue()).toEqual(value);
    expect(operator.getKey()).toEqual(key);
    expect(operator.getType()).toEqual(CommonDatabaseOperatorType.MATCH_SOME);
  });

  it("should create a join operator", async () => {
    const conditionBuilder = new CommonDatabaseConditionBuilder<TestModel>();
    const value = conditionBuilder.createEqualOperator("foo", "bar");
    const key = "bar";
    const operator = conditionBuilder.createJoinOperator(key, value);
    if (!operator) {
      throw new Error("creating should not return null");
    }

    expect(operator.getValue()).toEqual(value);
    expect(operator.getKey()).toEqual(key);
    expect(operator.getType()).toEqual(CommonDatabaseOperatorType.JOIN);
  });
});
