import { CommonDatabaseOperatorType } from "./CommonDatabaseOperatorType";
import { CommonLogicalDatabaseOperator } from "./CommonLogicalDatabaseOperator";
import { CommonRegExpDatabaseOperator } from "./CommonRegExpDatabaseOperator";

describe("CommonLogicalDatabaseOperator", () => {
  it("should return a nested common object", async () => {
    const childKey = `regexp`;
    const childValue = /foo/i;
    const childOperator = new CommonRegExpDatabaseOperator(
      childKey,
      childValue
    );

    const type = CommonDatabaseOperatorType.AND;
    const operator = new CommonLogicalDatabaseOperator(type, [childOperator]);

    const commonDatabaseOperatorObject = operator.toCommonObject();
    const expectedChildValue = childOperator.toCommonObject();
    expect(commonDatabaseOperatorObject).toEqual({
      type,
      value: [expectedChildValue],
    });
  });
});
