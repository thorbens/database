import { SearchOptions } from "../SearchOptions";
import {
  CommonDatabaseOperatorObject,
  isCommonDatabaseOperatorObject,
} from "./CommonDatabaseOperatorObject";

/**
 * Common Database Operator interface to create an class instance from.
 *
 * @typeparam T The model used for this operator object.
 */
export interface CommonSearchDatabaseOperatorObject
  extends CommonDatabaseOperatorObject {
  /**
   * The search options for this operator.
   */
  readonly options: SearchOptions;
}

/**
 * Checks if the given object provides all properties of CommonAtomDatabaseOperatorObject.
 *
 * @typeparam T The model used for the operator.
 * @param obj The object to check.
 */
export function isCommonSearchDatabaseOperatorObject(
  obj: object
): obj is CommonSearchDatabaseOperatorObject {
  return isCommonDatabaseOperatorObject(obj) && "options" in obj;
}
