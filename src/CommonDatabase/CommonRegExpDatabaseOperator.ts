import { RegExpObject } from "../Helper/isRegExpObject";
import { CommonAtomDatabaseOperator } from "./CommonAtomDatabaseOperator";
import { CommonAtomDatabaseOperatorObject } from "./CommonAtomDatabaseOperatorObject";
import { CommonDatabaseOperatorType } from "./CommonDatabaseOperatorType";
import { KeyModel } from "../KeyModel";

/**
 * A common reg exp database operator.
 */
export class CommonRegExpDatabaseOperator<
  TModel,
  TValue extends RegExp
> extends CommonAtomDatabaseOperator<TModel, TValue> {
  public constructor(key: KeyModel<TModel>, value: TValue) {
    super(CommonDatabaseOperatorType.REGEXP, key, value);
  }

  public toCommonObject(): CommonAtomDatabaseOperatorObject<TModel> {
    return {
      key: this.getKey(),
      type: this.getType(),
      value: this.toRegExpObject(),
    };
  }

  /**
   * Transforms this expression to a RegExpObject.
   */
  private toRegExpObject(): RegExpObject {
    const value = this.getValue() as RegExp;

    return {
      flags: value.flags,
      pattern: value.source,
    };
  }
}
