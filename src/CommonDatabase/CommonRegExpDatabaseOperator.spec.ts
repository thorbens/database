import { CommonDatabaseOperatorType } from "..";
import { CommonRegExpDatabaseOperator } from "./CommonRegExpDatabaseOperator";

describe("CommonRegExpDatabaseOperator", () => {
  it("should create the operator correctly", async () => {
    const value = /foo/i;
    const key = `bar`;
    const operator = new CommonRegExpDatabaseOperator(key, value);
    const regExpObject = {
      flags: value.flags,
      pattern: value.source,
    };

    expect(operator.getValue()).toEqual(value);
    expect(operator.getKey()).toEqual(key);
    expect(operator.getType()).toEqual(CommonDatabaseOperatorType.REGEXP);
    expect(operator.toObject()).toEqual({
      key,
      type: CommonDatabaseOperatorType.REGEXP,
      value: regExpObject,
    });
    expect(operator.toCommonObject()).toEqual({
      key,
      type: CommonDatabaseOperatorType.REGEXP,
      value: regExpObject,
    });
  });
});
