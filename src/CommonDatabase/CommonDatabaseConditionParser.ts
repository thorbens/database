import { AbstractDatabaseConditionParser } from "../AbstractDatabaseConditionParser";
import { DatabaseConditionBuilder } from "../DatabaseConditionBuilder";
import { CommonDatabaseConditionBuilder } from "./CommonDatabaseConditionBuilder";

/**
 * Common Database Condition Parser Implementation.
 * Uses a CommonDatabaseConditionBuilder for building conditions.
 */
export class CommonDatabaseConditionParser<
  TModel = {}
> extends AbstractDatabaseConditionParser<TModel> {
  /**
   * The used condition builder.
   */
  private readonly databaseConditionBuilder = new CommonDatabaseConditionBuilder<TModel>();
  /**
   * Regexp to match a full iso date.
   *
   * @see {@link https://stackoverflow.com/a/3143231}
   */
  private readonly dateRegExp = /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z)/;

  protected getConditionBuilder(): DatabaseConditionBuilder<TModel> {
    return this.databaseConditionBuilder;
  }

  protected isDate(value: string): boolean {
    return this.dateRegExp.test(value);
  }
}
