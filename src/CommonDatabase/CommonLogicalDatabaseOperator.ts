import { AbstractLogicalDatabaseOperator } from "../AbstractLogicalDatabaseOperator";
import { DatabaseOperator } from "../DatabaseOperator";
import { CommonDatabaseOperatorType } from "./CommonDatabaseOperatorType";

/**
 * A common database operator.
 */
export class CommonLogicalDatabaseOperator<
  TModel,
  TValue extends DatabaseOperator[] = DatabaseOperator[]
> extends AbstractLogicalDatabaseOperator<TModel, TValue> {
  /**
   * The type for this operator.
   */
  private readonly type: CommonDatabaseOperatorType;

  /**
   * Creates a new database operator with the given key and value.
   *
   * @param type The type of this operator.
   * @param value The value to set.
   */
  public constructor(type: CommonDatabaseOperatorType, value: TValue) {
    super(value);
    this.type = type;
  }

  public toObject(): object {
    return this.toCommonObject();
  }

  public getType(): CommonDatabaseOperatorType {
    return this.type;
  }
}
