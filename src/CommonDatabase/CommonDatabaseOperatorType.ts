/**
 * List of common database operator types.
 */
export enum CommonDatabaseOperatorType {
  /**
   * Bool operator.
   */
  BOOL = "bool",
  /**
   * And operator.
   */
  AND = "and",
  /**
   * Or operator.
   */
  OR = "or",
  /**
   * Regular expression operator.
   */
  REGEXP = "regexp",
  /**
   * Equal operator.
   */
  EQUAL = "equal",
  /**
   * Not equal operator.
   */
  NOT_EQUAL = "not_equal",
  /**
   * In operator.
   */
  IN = "in",
  /**
   * Match some operator.
   */
  MATCH_SOME = "match_some",
  /**
   * Not operator.
   */
  NOT = "not",
  /**
   * Exists operator.
   */
  EXISTS = "exists",
  /**
   * Greater than operator.
   */
  GT = "gt",
  /**
   * Greater than or equal operator.
   */
  GTE = "gte",
  /**
   * Less than operator.
   */
  LT = "lt",
  /**
   * Less than or equal operator.
   */
  LTE = "gte",
  /**
   * Full Text search operator
   */
  FULL_TEXT = "full_text",
  /**
   * Join operator
   */
  JOIN = "join",
}
