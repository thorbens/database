import { AbstractAtomDatabaseOperator } from "../AbstractAtomDatabaseOperator";
import { AtomDatabaseOperator } from "../AtomDatabaseOperator";
import { CommonDatabaseOperatorType } from "./CommonDatabaseOperatorType";
import { KeyModel } from "../KeyModel";

/**
 * A common database operator.
 *
 * @typeparam T The model used for this operator.
 */
export class CommonAtomDatabaseOperator<TModel, TValue = unknown>
  extends AbstractAtomDatabaseOperator<TModel, TValue>
  implements AtomDatabaseOperator<TModel, TValue> {
  /**
   * The type for this operator.
   */
  private readonly type: CommonDatabaseOperatorType;

  /**
   * Creates a new database operator with the given key and value.
   *
   * @param type The type of this operator.
   * @param key The key to set.
   * @param value The value to set.
   */
  public constructor(
    type: CommonDatabaseOperatorType,
    key: KeyModel<TModel>,
    value: TValue
  ) {
    super(key, value);
    this.type = type;
  }

  public toObject(): object {
    return this.toCommonObject();
  }

  public getType(): CommonDatabaseOperatorType {
    return this.type;
  }
}
