import { CommonDatabaseOperatorType } from "..";
import { CommonAtomDatabaseOperator } from "./CommonAtomDatabaseOperator";
import { CommonRegExpDatabaseOperator } from "./CommonRegExpDatabaseOperator";

describe("CommonAtomDatabaseOperator", () => {
  it("should create the operator correctly", async () => {
    const value = `foo`;
    const key = `bar`;
    const type = CommonDatabaseOperatorType.EQUAL;
    const operator = new CommonAtomDatabaseOperator(type, key, value);

    expect(operator.getValue()).toEqual(value);
    expect(operator.getKey()).toEqual(key);
    expect(operator.getType()).toEqual(type);
    expect(operator.toObject()).toEqual({
      key,
      type,
      value,
    });
    expect(operator.toCommonObject()).toEqual({
      key,
      type,
      value,
    });
  });

  it("should return a nested common object", async () => {
    const childKey = `regexp`;
    const childValue = /foo/i;
    const childOperator = new CommonRegExpDatabaseOperator(
      childKey,
      childValue
    );

    const key = `bar`;
    const type = CommonDatabaseOperatorType.EQUAL;
    const operator = new CommonAtomDatabaseOperator(type, key, childOperator);

    expect(operator.toCommonObject()).toEqual({
      key,
      type,
      value: childOperator.toCommonObject(),
    });
  });
});
