import { SearchOptions } from "../SearchOptions";
import { CommonDatabaseOperatorType } from "./CommonDatabaseOperatorType";
import { CommonSearchDatabaseOperator } from "./CommonSearchDatabaseOperator";

describe("CommonSearchDatabaseOperator", () => {
  it("should create the operator correctly", async () => {
    const options: SearchOptions = {
      fields: [`foo`],
    };
    const searchTerm = `bar`;
    const operator = new CommonSearchDatabaseOperator(
      CommonDatabaseOperatorType.FULL_TEXT,
      searchTerm,
      options
    );

    expect(operator.getValue()).toEqual(searchTerm);
    expect(operator.getOptions()).toEqual(options);
    expect(operator.getType()).toEqual(CommonDatabaseOperatorType.FULL_TEXT);
    expect(operator.toObject()).toEqual({
      options,
      type: CommonDatabaseOperatorType.FULL_TEXT,
      value: searchTerm,
    });
    expect(operator.toCommonObject()).toEqual({
      options,
      type: CommonDatabaseOperatorType.FULL_TEXT,
      value: searchTerm,
    });
  });
});
