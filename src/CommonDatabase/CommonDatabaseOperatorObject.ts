import { CommonDatabaseOperatorType } from "./CommonDatabaseOperatorType";

/**
 * Common Database Operator interface to create an class instance from.
 */
export interface CommonDatabaseOperatorObject {
  /**
   * The type for this operator.
   */
  readonly type: CommonDatabaseOperatorType;
  /**
   * The value for this operator.
   */
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  readonly value: any;
}

/**
 * Checks if the given object provides all properties of CommonDatabaseOperatorObject.
 *
 * @param obj The object to check.
 */
export function isCommonDatabaseOperatorObject(
  obj: object
): obj is CommonDatabaseOperatorObject {
  return obj && "type" in obj && "value" in obj;
}
