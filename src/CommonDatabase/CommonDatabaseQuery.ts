import { AbstractDatabaseQuery } from "../AbstractDatabaseQuery";

/**
 * Common Database Query Implementation.
 */
export class CommonDatabaseQuery<
  TSource,
  TResult = TSource
> extends AbstractDatabaseQuery<TSource, TResult> {}
