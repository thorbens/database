import { AbstractDatabaseConditionBuilder } from "../AbstractDatabaseConditionBuilder";
import { AtomDatabaseOperator } from "../AtomDatabaseOperator";
import { DatabaseOperator } from "../DatabaseOperator";
import { LogicalDatabaseOperator } from "../LogicalDatabaseOperator";
import { SearchDatabaseOperator } from "../SearchDatabaseOperator";
import { SearchOptions } from "../SearchOptions";
import { CommonAtomDatabaseOperator } from "./CommonAtomDatabaseOperator";
import { CommonDatabaseOperatorType } from "./CommonDatabaseOperatorType";
import { CommonLogicalDatabaseOperator } from "./CommonLogicalDatabaseOperator";
import { CommonRegExpDatabaseOperator } from "./CommonRegExpDatabaseOperator";
import { CommonSearchDatabaseOperator } from "./CommonSearchDatabaseOperator";
import { KeyModel } from "../KeyModel";

/**
 * Common Database Condition Builder implementation.
 * Every Operator will be created as CommonAtomDatabaseOperator with the according type.
 */
export class CommonDatabaseConditionBuilder<
  TModel = {}
> extends AbstractDatabaseConditionBuilder<TModel> {
  public createEqualOperator<TValue = unknown>(
    key: KeyModel<TModel>,
    value: TValue
  ): AtomDatabaseOperator<TModel, TValue> {
    return this.createCommonAtomOperator(
      CommonDatabaseOperatorType.EQUAL,
      key,
      value
    );
  }

  public createNotEqualOperator<TValue = unknown>(
    key: KeyModel<TModel>,
    value: TValue
  ): AtomDatabaseOperator<TModel, TValue> {
    return this.createCommonAtomOperator(
      CommonDatabaseOperatorType.NOT_EQUAL,
      key,
      value
    );
  }

  public createNotOperator<
    TValue extends DatabaseOperator[] = DatabaseOperator[]
  >(value: TValue): LogicalDatabaseOperator<TModel, TValue> {
    return this.createCommonLogicalOperator(
      CommonDatabaseOperatorType.NOT,
      value
    );
  }

  public createMatchSomeOperator<TValue = unknown>(
    key: KeyModel<TModel>,
    value: TValue
  ): AtomDatabaseOperator<TModel, TValue> {
    return this.createCommonAtomOperator(
      CommonDatabaseOperatorType.MATCH_SOME,
      key,
      value
    );
  }

  public createInOperator<TValue extends unknown[] = []>(
    key: KeyModel<TModel>,
    value: TValue
  ): AtomDatabaseOperator<TModel, TValue> {
    return this.createCommonAtomOperator(
      CommonDatabaseOperatorType.IN,
      key,
      value
    );
  }

  public createGreaterThanOperator<TValue = unknown>(
    key: KeyModel<TModel>,
    value: TValue
  ): AtomDatabaseOperator<TModel, TValue> {
    return this.createCommonAtomOperator(
      CommonDatabaseOperatorType.GT,
      key,
      value
    );
  }

  public createGreaterThanOrEqualOperator<TValue = unknown>(
    key: KeyModel<TModel>,
    value: TValue
  ): AtomDatabaseOperator<TModel, TValue> {
    return this.createCommonAtomOperator(
      CommonDatabaseOperatorType.GTE,
      key,
      value
    );
  }

  public createLessThanOperator<TValue = unknown>(
    key: KeyModel<TModel>,
    value: TValue
  ): AtomDatabaseOperator<TModel, TValue> {
    return this.createCommonAtomOperator(
      CommonDatabaseOperatorType.LT,
      key,
      value
    );
  }

  public createLessThanOrEqualOperator<TValue = unknown>(
    key: KeyModel<TModel>,
    value: TValue
  ): AtomDatabaseOperator<TModel, TValue> {
    return this.createCommonAtomOperator(
      CommonDatabaseOperatorType.LTE,
      key,
      value
    );
  }

  public createExistsOperator(
    key: KeyModel<TModel>,
    value: DatabaseOperator[]
  ): AtomDatabaseOperator<TModel> {
    return this.createCommonAtomOperator(
      CommonDatabaseOperatorType.EXISTS,
      key,
      value
    );
  }

  public createJoinOperator(
    name: KeyModel<TModel>,
    condition: DatabaseOperator
  ): AtomDatabaseOperator<TModel> {
    return this.createCommonAtomOperator(
      CommonDatabaseOperatorType.JOIN,
      name,
      condition
    );
  }

  public createAndOperator(
    value: DatabaseOperator[]
  ): LogicalDatabaseOperator<TModel> {
    return this.createCommonLogicalOperator(
      CommonDatabaseOperatorType.AND,
      value
    );
  }

  public createOrOperator(
    value: DatabaseOperator[]
  ): LogicalDatabaseOperator<TModel> {
    return this.createCommonLogicalOperator(
      CommonDatabaseOperatorType.OR,
      value
    );
  }

  public createFullTextOperator<TValue extends string = string>(
    searchTerm: TValue,
    options?: SearchOptions
  ): SearchDatabaseOperator<TValue> {
    return this.createCommonSearchOperator(
      CommonDatabaseOperatorType.FULL_TEXT,
      searchTerm,
      options || {}
    );
  }

  public createRegExpOperator<TValue extends RegExp = RegExp>(
    key: KeyModel<TModel>,
    value: TValue
  ): AtomDatabaseOperator<TModel, TValue> {
    return new CommonRegExpDatabaseOperator(key, value);
  }

  /**
   * Creates a common search operator with the given type, searchTerm and options.
   *
   * @param type The type of the operator.
   * @param searchTerm
   * @param options
   */
  private createCommonSearchOperator<TValue extends string = string>(
    type: CommonDatabaseOperatorType,
    searchTerm: TValue,
    options: SearchOptions
  ): SearchDatabaseOperator<TValue> {
    return new CommonSearchDatabaseOperator(type, searchTerm, options);
  }

  /**
   * Creates a common atom operator with the given type, key and value.
   *
   * @param type The type of the operator.
   * @param key The key of thw operator.
   * @param value The value of the operator.
   */
  private createCommonAtomOperator<TValue = unknown>(
    type: CommonDatabaseOperatorType,
    key: KeyModel<TModel>,
    value: TValue
  ): AtomDatabaseOperator<TModel, TValue> {
    return new CommonAtomDatabaseOperator(type, key, value);
  }

  /**
   * Creates a common logical operator with the given type, key and value.
   *
   * @param type The type of the operator.
   * @param value The value of the operator.
   */
  private createCommonLogicalOperator<
    TValue extends DatabaseOperator[] = DatabaseOperator[]
  >(
    type: CommonDatabaseOperatorType,
    value: TValue
  ): LogicalDatabaseOperator<TModel, TValue> {
    return new CommonLogicalDatabaseOperator(type, value);
  }
}
