import { DatabaseOrderByDirection } from "../DatabaseOrderByDirection";
import { CommonDatabaseConditionBuilder } from "./CommonDatabaseConditionBuilder";
import { CommonDatabaseQuery } from "./CommonDatabaseQuery";

interface TestModel {
  foo: string;
  bar: string;
}

describe("CommonDatabaseQuery", () => {
  it("should set and retrieve correctly", async () => {
    const query = new CommonDatabaseQuery();
    // check default values
    expect(query.getLimit()).toEqual(0);
    expect(query.getOffset()).toEqual(0);
    expect(query.getOrderBy()).toEqual(null);
    expect(query.getOrderByDirection()).toEqual(DatabaseOrderByDirection.ASC);

    const limit = 100;
    query.setLimit(limit);
    expect(query.getLimit()).toEqual(limit);

    const offset = 25;
    query.setOffset(offset);
    expect(query.getOffset()).toEqual(offset);

    const orderBy = `foo`;
    query.setOrderBy(orderBy);
    expect(query.getOrderBy()).toEqual(orderBy);

    const orderByDirection = DatabaseOrderByDirection.DESC;
    query.setOrderByDirection(orderByDirection);
    expect(query.getOrderByDirection()).toEqual(orderByDirection);
  });

  it("should set and clear conditions correctly", async () => {
    const query = new CommonDatabaseQuery();
    // by default, there should not be any conditions.
    expect(query.hasConditions()).toEqual(false);
    // check the generated object
    const object = query.toObject();
    // no conditions should lead to an empty object
    expect(object).toEqual({});

    // add first conditions
    const conditionBuilder = new CommonDatabaseConditionBuilder<TestModel>();
    const equalCondition = conditionBuilder.createEqualOperator("foo", "bar");

    query.addCondition(equalCondition);
    expect(query.hasConditions()).toEqual(true);
    // check the generate object
    expect(query.toObject()).toEqual(equalCondition.toObject());

    // clear conditions
    query.clearConditions();
    // no conditions should be left
    expect(query.hasConditions()).toEqual(false);
  });
});
