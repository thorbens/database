import {
  AtomDatabaseOperator,
  CommonDatabaseConditionBuilder,
  CommonDatabaseOperatorObject,
  CommonDatabaseOperatorType,
} from "..";
import { SearchDatabaseOperator } from "../SearchDatabaseOperator";
import { SearchOptions } from "../SearchOptions";
import { CommonAtomDatabaseOperator } from "./CommonAtomDatabaseOperator";
import { CommonAtomDatabaseOperatorObject } from "./CommonAtomDatabaseOperatorObject";
import { CommonDatabaseConditionParser } from "./CommonDatabaseConditionParser";
import { CommonSearchDatabaseOperatorObject } from "./CommonSearchDatabaseOperatorObject";

interface TestModel {
  bar: string;
}

describe("CommonDatabaseConditionParser", () => {
  it("should parse a simple object correctly", async () => {
    const conditionParser = new CommonDatabaseConditionParser();
    const key = `foo`;
    const value = `bar`;
    const type = CommonDatabaseOperatorType.EQUAL;
    const operatorObject: CommonAtomDatabaseOperatorObject<never> = {
      key,
      type,
      value,
    };
    const operator = conditionParser.parse(
      operatorObject
    ) as AtomDatabaseOperator;
    if (!operator) {
      throw new Error(`parsing should not return null`);
    }

    expect(operator.getKey()).toEqual(key);
    expect(operator.getValue()).toEqual(value);
    expect(operator.getType()).toEqual(type);
  });

  it("should parse nested conditions correctly", async () => {
    const conditionParser = new CommonDatabaseConditionParser();
    const nestedKey = `nested`;
    const nestedValue = `nestedValue`;
    const nestedType = CommonDatabaseOperatorType.EQUAL;
    const nestedOperatorObject: CommonAtomDatabaseOperatorObject<never> = {
      key: nestedKey,
      type: nestedType,
      value: nestedValue,
    };
    const type = CommonDatabaseOperatorType.AND;
    const operatorObject: CommonDatabaseOperatorObject = {
      type,
      value: nestedOperatorObject,
    };
    const operator = conditionParser.parse(operatorObject);
    if (!operator) {
      throw new Error(`parsing should not return null`);
    }

    expect(operator.getType()).toEqual(type);
    const parsedValue = operator.getValue() as CommonAtomDatabaseOperator<never>;
    expect(operator.getValue() instanceof CommonAtomDatabaseOperator).toEqual(
      true
    );
    expect(parsedValue.getValue()).toEqual(nestedValue);
    expect(parsedValue.getKey()).toEqual(nestedKey);
    expect(parsedValue.getType()).toEqual(nestedType);
  });

  it("should parse a regexp operator correctly", async () => {
    const conditionParser = new CommonDatabaseConditionParser();
    const key = `foo`;
    const regExp = /foo/i;
    // set the regexp as object
    const value = {
      flags: regExp.flags,
      pattern: regExp.source,
    };
    const type = CommonDatabaseOperatorType.REGEXP;
    const operatorObject: CommonAtomDatabaseOperatorObject<never> = {
      key,
      type,
      value,
    };
    const operator = conditionParser.parse<RegExp>(operatorObject);
    if (!operator) {
      throw new Error(`parsing should not return null`);
    }

    expect((operator as AtomDatabaseOperator).getKey()).toEqual(key);
    expect(operator.getValue().toString()).toEqual(regExp.toString());
    expect(operator.getType()).toEqual(type);
  });

  it("should parse a regexp operator correctly produced by a condition builder", async () => {
    const conditionBuilder = new CommonDatabaseConditionBuilder<TestModel>();
    const value = /foo/i;
    const key = "bar";
    const operator = conditionBuilder.createRegExpOperator(key, value);
    if (!operator) {
      throw new Error(`creating should not return null`);
    }

    const conditionParser = new CommonDatabaseConditionParser();
    const commonObject = operator.toCommonObject();
    const parsedOperator = conditionParser.parse(
      commonObject
    ) as AtomDatabaseOperator<never, RegExp>;
    if (!operator) {
      throw new Error(`parsing should not return null`);
    }

    expect(parsedOperator.getKey()).toEqual(operator.getKey());
    const parsedValue = parsedOperator.getValue();
    expect(parsedValue instanceof RegExp).toEqual(true);
    expect(parsedValue.toString()).toEqual(operator.getValue().toString());
    expect(parsedOperator.getType()).toEqual(operator.getType());
  });

  it("should parse a date string correctly", async () => {
    const conditionParser = new CommonDatabaseConditionParser();
    const key = `foo`;
    const value = new Date().toISOString();
    const type = CommonDatabaseOperatorType.EQUAL;
    const operatorObject: CommonAtomDatabaseOperatorObject<never> = {
      key,
      type,
      value,
    };
    const operator = conditionParser.parse(
      operatorObject
    ) as AtomDatabaseOperator<never, Date>;
    if (!operator) {
      throw new Error(`parsing should not return null`);
    }

    expect(operator.getKey()).toEqual(key);
    expect(operator.getValue() instanceof Date).toEqual(true);
    expect(operator.getValue().toISOString()).toEqual(value);
    expect(operator.getType()).toEqual(type);
  });

  it("should parse a full text operator correctly", async () => {
    const conditionParser = new CommonDatabaseConditionParser();
    const options: SearchOptions = {
      fields: [`foo`],
    };
    const value = "bar";
    const type = CommonDatabaseOperatorType.FULL_TEXT;
    const operatorObject: CommonSearchDatabaseOperatorObject = {
      options,
      type,
      value,
    };
    const operator = conditionParser.parse(
      operatorObject
    ) as SearchDatabaseOperator;
    if (!operator) {
      throw new Error(`parsing should not return null`);
    }

    expect(operator.getValue()).toEqual(value);
    expect(operator.getOptions()).toEqual(options);
    expect(operator.getType()).toEqual(type);
  });
});
