import { AbstractDatabaseOperator } from "./AbstractDatabaseOperator";
import { AtomDatabaseOperator } from "./AtomDatabaseOperator";
import { DatabaseOperator } from "./DatabaseOperator";
import { LogicalDatabaseOperator } from "./LogicalDatabaseOperator";

/**
 * Abstract implementation for an logical database operator.
 * Extend this class to implement database specific logical operators.
 *
 * @typeparam TModel The model used for this operator.
 * @typeparam TValue The value used for this operator.
 */
export abstract class AbstractLogicalDatabaseOperator<
    TModel,
    TValue extends DatabaseOperator[] = DatabaseOperator[]
  >
  extends AbstractDatabaseOperator<TValue>
  implements LogicalDatabaseOperator<TModel, TValue> {
  /**
   * Creates a new database operator with the given key and value.
   *
   * @param value The value to set.
   */
  public constructor(value: TValue) {
    super(value);
  }

  public addCondition(
    condition: AtomDatabaseOperator<TModel> | LogicalDatabaseOperator<TModel>
  ): void {
    this.getValue().push(condition);
  }
}
