import {
  CommonDatabaseOperatorObject,
  CommonDatabaseOperatorType,
} from "./CommonDatabase";

/**
 * Represents a condition for a query where the key should match the given value.
 * To transform this condition to a suitable representation, use the toObject method.
 */
export interface DatabaseOperator<TValue = unknown> {
  /**
   * Returns the value for this condition.
   */
  getValue(): TValue;

  /**
   * Returns the object representation for this condition.
   */
  toObject(): object;

  /**
   * Returns the type of this operator.
   */
  getType(): CommonDatabaseOperatorType;

  /**
   * Returns a common object for this operator.
   */
  toCommonObject(): CommonDatabaseOperatorObject;
}
