/**
 * Represents search options for a {@link SearchDatabaseOperator}.
 */
export interface SearchOptions {
  /**
   * The fields which should be searched.
   */
  fields?: string[];
}
