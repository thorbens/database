/**
 * Database order direction.
 */
export enum DatabaseOrderByDirection {
  /**
   * Ascending direction.
   */
  ASC,
  /**
   * Descending direction.
   */
  DESC,
}
