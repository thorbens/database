import { AbstractDatabaseOperator } from "./AbstractDatabaseOperator";
import { AtomDatabaseOperator } from "./AtomDatabaseOperator";
import { CommonAtomDatabaseOperatorObject } from "./CommonDatabase";
import { KeyModel } from "./KeyModel";

/**
 * Abstract implementation for an atom database operator.
 * Extend this class to implement database specific atom operators.
 */
export abstract class AbstractAtomDatabaseOperator<TModel, TValue = unknown>
  extends AbstractDatabaseOperator<TValue>
  implements AtomDatabaseOperator<TModel, TValue> {
  /**
   * The key for this operator.
   */
  private readonly key: KeyModel<TModel>;

  /**
   * Creates a new database operator with the given key and value.
   *
   * @param key The key to set.
   * @param value The value to set.
   */
  public constructor(key: KeyModel<TModel>, value: TValue) {
    super(value);
    this.key = key;
  }

  public abstract toObject(): object;

  public toCommonObject(): CommonAtomDatabaseOperatorObject<TModel> {
    return {
      key: this.getKey(),
      ...super.toCommonObject(),
    };
  }

  public getKey(): KeyModel<TModel> {
    return this.key;
  }
}
