import { AtomDatabaseOperator } from "./AtomDatabaseOperator";
import { CommonDatabaseOperatorType } from "./CommonDatabase";
import { DatabaseOperator } from "./DatabaseOperator";
import { LogicalDatabaseOperator } from "./LogicalDatabaseOperator";
import { SearchDatabaseOperator } from "./SearchDatabaseOperator";
import { SearchOptions } from "./SearchOptions";
import { KeyModel } from "./KeyModel";

/**
 * Provides functionality to build new conditions (factory).
 */
export interface DatabaseConditionBuilder<TModel = {}> {
  /**
   * Builds an equal operator.
   *
   * @param key The key to use.
   * @param value The value to compare.
   */
  createEqualOperator<TValue = unknown>(
    key: KeyModel<TModel>,
    value: TValue
  ): AtomDatabaseOperator<TModel, TValue>;

  /**
   * Builds an not equal operator.
   *
   * @param key The key to use.
   * @param value The value to compare.
   */
  createNotEqualOperator<TValue = unknown>(
    key: KeyModel<TModel>,
    value: TValue
  ): AtomDatabaseOperator<TModel, TValue>;

  /**
   * Builds an not operator.
   *
   * @param value The value to compare.
   */
  createNotOperator(value: DatabaseOperator[]): LogicalDatabaseOperator<TModel>;

  /**
   * Builds a match some operator.
   * The condition is used if a value should match at least one element in a list.
   *
   * @param key The key to use.
   * @param value The value to compare.
   */
  createMatchSomeOperator<TValue = unknown>(
    key: KeyModel<TModel>,
    value: TValue
  ): AtomDatabaseOperator<TModel, TValue>;

  /**
   * Builds an in operator where the value of a field equals any value in the specified array.
   *
   * @param key
   * @param value
   */
  createInOperator<TValue extends unknown[] = []>(
    key: KeyModel<TModel>,
    value: TValue
  ): AtomDatabaseOperator<TModel, TValue>;

  /**
   * Builds a greater than operator.
   *
   * @param key The key to use.
   * @param value The value to compare.
   */
  createGreaterThanOperator<TValue = unknown>(
    key: KeyModel<TModel>,
    value: TValue
  ): AtomDatabaseOperator<TModel, TValue>;

  /**
   * Builds a greater than or equal operator.
   *
   * @param key The key to use.
   * @param value The value to compare.
   */
  createGreaterThanOrEqualOperator<TValue = unknown>(
    key: KeyModel<TModel>,
    value: TValue
  ): AtomDatabaseOperator<TModel, TValue>;

  /**
   * Builds a less than operator.
   *
   * @param key The key to use.
   * @param value The value to compare.
   */
  createLessThanOperator<TValue = unknown>(
    key: KeyModel<TModel>,
    value: TValue
  ): AtomDatabaseOperator<TModel, TValue>;

  /**
   * Builds a less than or equal operator.
   *
   * @param key The key to use.
   * @param value The value to compare.
   */
  createLessThanOrEqualOperator<TValue = unknown>(
    key: KeyModel<TModel>,
    value: TValue
  ): AtomDatabaseOperator<TModel, TValue>;

  /**
   * Builds a exists operator.
   *
   * @param key The key to use.
   * @param value The value to compare.
   */
  createExistsOperator<TValue extends DatabaseOperator[] = DatabaseOperator[]>(
    key: KeyModel<TModel>,
    value: TValue
  ): AtomDatabaseOperator<TModel>;

  /**
   * Builds a operator for a regular expression.
   *
   * @param key The key to use.
   * @param regExp the regular expression to use.
   */
  createRegExpOperator(
    key: KeyModel<TModel>,
    regExp: RegExp
  ): AtomDatabaseOperator<TModel>;

  /**
   * Builds a logical and operator.
   *
   * @param value
   */
  createAndOperator(value: DatabaseOperator[]): LogicalDatabaseOperator<TModel>;

  /**
   * Builds a logical or operator.
   *
   * @param value
   */
  createOrOperator(value: DatabaseOperator[]): LogicalDatabaseOperator<TModel>;

  /**
   * Creates a full text operator.
   *
   * @param searchTerm The search term.
   * @param options Additional search options.
   */
  createFullTextOperator<TValue extends string = string>(
    searchTerm: TValue,
    options?: SearchOptions
  ): SearchDatabaseOperator<TValue>;

  /**
   * Creates an operator by the given type.
   * If no condition could be created, returns null.
   *
   * @param type The type of operator to create.
   * @param key
   * @param value
   */
  createAtomOperatorByType<TValue = unknown>(
    type: CommonDatabaseOperatorType,
    key: KeyModel<TModel>,
    value: TValue
  ): AtomDatabaseOperator<TModel, TValue> | null;

  /**
   * Creates an operator by the given type.
   * If no condition could be created, returns null.
   *
   * @param type The type of operator to create.
   * @param value
   */
  createLogicalOperatorByType(
    type: CommonDatabaseOperatorType,
    value: DatabaseOperator[]
  ): LogicalDatabaseOperator<TModel> | null;

  /**
   * Creates a search operator for the given type.
   *
   * @param type The type of operator to create.
   * @param searchTerm The search term to set.
   * @param options The options to pass to the created operator.
   */
  createSearchOperatorByType<TValue extends string = string>(
    type: CommonDatabaseOperatorType,
    searchTerm: TValue,
    options: SearchOptions
  ): SearchDatabaseOperator<TValue> | null;

  /**
   * Creates a join operator for the field.
   *
   * @param name The name of the joined table / field.
   * @param condition The condition to apply to the join (use a logical condition to chain operations).
   */
  createJoinOperator(
    name: KeyModel<TModel>,
    condition: DatabaseOperator
  ): AtomDatabaseOperator<TModel>;
}
