export type KeyModel<TModel> = keyof TModel | string;
