import {
  CommonDatabaseOperatorObject,
  CommonDatabaseOperatorType,
} from "./CommonDatabase";
import { DatabaseOperator } from "./DatabaseOperator";

/**
 * Abstract implementation for a DatabaseOperator.
 * Provides a basic constructor as basic functionality for retrieving the key and the value.
 * Also, a getConditionValue allows to retrieve the value as object if the value is a condition.
 *
 * @typeparam TValue The value for this operator.
 */
export abstract class AbstractDatabaseOperator<TValue = unknown>
  implements DatabaseOperator<TValue> {
  /**
   * The value for this database operator.
   */
  private readonly value: TValue;

  /**
   * Creates a new database operator with the given key and value.
   *
   * @param value The value to set.
   */
  public constructor(value: TValue) {
    this.value = value;
  }

  public getValue(): TValue {
    return this.value;
  }

  public abstract toObject(): object;

  public abstract getType(): CommonDatabaseOperatorType;

  public toCommonObject(): CommonDatabaseOperatorObject {
    const unknownValue = this.getValue();

    let value;
    if (Array.isArray(unknownValue)) {
      value = unknownValue.map(this.getConditionCommonValueByValue);
    } else if (isCondition(unknownValue)) {
      value = unknownValue.toCommonObject();
    } else {
      value = unknownValue as TValue;
    }

    return {
      type: this.getType(),
      value,
    };
  }

  /**
   * Checks if the value passed for this condition is a condition as well.
   * If so, returns the object for this condition.
   */
  protected getConditionValue(): object {
    if (Array.isArray(this.value)) {
      // if value is an array, check each element of the array itself
      return this.value.map(this.getConditionValueByValue);
    }

    return this.getConditionValueByValue(this.value);
  }

  /**
   * Checks if the given value is a condition value.
   * If so, returns the object for this value, otherwise the value itself.
   *
   * @param value The value to check.
   */
  protected getConditionValueByValue(value: unknown): object {
    return isCondition(value) ? value.toObject() : (value as object);
  }

  /**
   * Checks if the given value is a condition value.
   * If so, returns the common object for this value, otherwise the value itself.
   *
   * @param value The value to check.
   */
  protected getConditionCommonValueByValue(value: object): object {
    return isCondition(value) ? value.toCommonObject() : value;
  }
}

/**
 * Checks if the given object is a condition.
 *
 * @param obj The object to check.
 */
export function isCondition(obj: unknown): obj is DatabaseOperator {
  return obj instanceof AbstractDatabaseOperator;
}
