import { AtomDatabaseOperator } from "./AtomDatabaseOperator";
import { CommonDatabaseOperatorType } from "./CommonDatabase";
import { DatabaseConditionBuilder } from "./DatabaseConditionBuilder";
import { DatabaseOperator } from "./DatabaseOperator";
import { LogicalDatabaseOperator } from "./LogicalDatabaseOperator";
import { SearchDatabaseOperator } from "./SearchDatabaseOperator";
import { SearchOptions } from "./SearchOptions";
import { KeyModel } from "./KeyModel";

/**
 * Abstract implementation for a condition builder.
 * New Condition Builders should extend this class.
 */
export abstract class AbstractDatabaseConditionBuilder<TModel = {}>
  implements DatabaseConditionBuilder<TModel> {
  public abstract createEqualOperator<TValue = unknown>(
    key: KeyModel<TModel>,
    value: TValue
  ): AtomDatabaseOperator<TModel, TValue>;

  public abstract createNotEqualOperator<TValue = unknown>(
    key: KeyModel<TModel>,
    value: TValue
  ): AtomDatabaseOperator<TModel, TValue>;

  public abstract createNotOperator(
    value: DatabaseOperator[]
  ): LogicalDatabaseOperator<TModel>;

  public abstract createMatchSomeOperator<TValue = unknown>(
    key: KeyModel<TModel>,
    value: TValue
  ): AtomDatabaseOperator<TModel, TValue>;

  public abstract createInOperator<TValue extends unknown[] = []>(
    key: KeyModel<TModel>,
    value: TValue
  ): AtomDatabaseOperator<TModel, TValue>;

  public abstract createGreaterThanOperator<TValue = unknown>(
    key: KeyModel<TModel>,
    value: TValue
  ): AtomDatabaseOperator<TModel, TValue>;

  public abstract createGreaterThanOrEqualOperator<TValue = unknown>(
    key: KeyModel<TModel>,
    value: TValue
  ): AtomDatabaseOperator<TModel, TValue>;

  public abstract createLessThanOperator<TValue = unknown>(
    key: KeyModel<TModel>,
    value: TValue
  ): AtomDatabaseOperator<TModel, TValue>;

  public abstract createLessThanOrEqualOperator<TValue = unknown>(
    key: KeyModel<TModel>,
    value: TValue
  ): AtomDatabaseOperator<TModel, TValue>;

  public abstract createExistsOperator<
    TValue extends DatabaseOperator[] = DatabaseOperator[]
  >(key: KeyModel<TModel>, value: TValue): AtomDatabaseOperator<TModel>;

  public createAtomOperatorByType<TValue = unknown>(
    type: CommonDatabaseOperatorType,
    key: KeyModel<TModel>,
    value: TValue
  ): AtomDatabaseOperator<TModel, TValue> | null {
    switch (type) {
      case CommonDatabaseOperatorType.EQUAL:
        return this.createEqualOperator(key, value);
      case CommonDatabaseOperatorType.REGEXP:
        if (!(value instanceof RegExp)) {
          throw new Error("value must be an instance of RegExg");
        }

        return this.createRegExpOperator(key, value);
      case CommonDatabaseOperatorType.MATCH_SOME:
        return this.createMatchSomeOperator(key, value);
      case CommonDatabaseOperatorType.IN:
        if (!Array.isArray(value)) {
          throw new Error("value must be an array");
        }

        return this.createInOperator(key, value);
      case CommonDatabaseOperatorType.LT:
        return this.createLessThanOperator(key, value);
      case CommonDatabaseOperatorType.GT:
        return this.createGreaterThanOperator(key, value);
      case CommonDatabaseOperatorType.EXISTS:
        if (!Array.isArray(value)) {
          throw new Error("value must be an array");
        }

        return this.createExistsOperator(key, value) as AtomDatabaseOperator<
          TModel,
          TValue
        >;
    }

    return null;
  }

  public createLogicalOperatorByType(
    type: CommonDatabaseOperatorType,
    value: DatabaseOperator[]
  ): LogicalDatabaseOperator<TModel> | null {
    switch (type) {
      case CommonDatabaseOperatorType.OR:
        return this.createOrOperator(value);
      case CommonDatabaseOperatorType.AND:
        return this.createAndOperator(value);
    }

    return null;
  }

  public createSearchOperatorByType<TValue extends string = string>(
    type: CommonDatabaseOperatorType,
    searchTerm: TValue,
    options: SearchOptions
  ): SearchDatabaseOperator<TValue> | null {
    switch (type) {
      case CommonDatabaseOperatorType.FULL_TEXT:
        return this.createFullTextOperator(searchTerm, options);
    }

    return null;
  }

  public abstract createJoinOperator(
    name: KeyModel<TModel>,
    condition: DatabaseOperator<TModel>
  ): AtomDatabaseOperator<TModel>;

  public abstract createAndOperator(
    value: DatabaseOperator[]
  ): LogicalDatabaseOperator<TModel>;

  public abstract createOrOperator(
    value: DatabaseOperator[]
  ): LogicalDatabaseOperator<TModel>;

  public abstract createFullTextOperator<TValue extends string = string>(
    searchTerm: TValue,
    options?: SearchOptions
  ): SearchDatabaseOperator<TValue>;

  public abstract createRegExpOperator<TValue extends RegExp = RegExp>(
    key: KeyModel<TModel>,
    regExp: TValue
  ): AtomDatabaseOperator<TModel, TValue>;
}
