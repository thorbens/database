import { CommonDatabaseOperatorObject } from "./CommonDatabase";
import { DatabaseOperator } from "./DatabaseOperator";
import { LogicalDatabaseOperator } from "./LogicalDatabaseOperator";

/**
 * Parses given conditions and create according operators.
 */
export interface DatabaseConditionParser<TModel = {}> {
  /**
   * Parses the given conditions to database operators.
   * If no condition could be parsed, returns null.
   *
   * @param condition The condition to parse.
   */
  parse<TValue = unknown>(
    condition: CommonDatabaseOperatorObject
  ): DatabaseOperator<TValue> | LogicalDatabaseOperator<TModel> | null;
}
