import {
  isCommonAtomDatabaseOperatorObject,
  isCommonSearchDatabaseOperatorObject,
  CommonDatabaseOperatorObject,
  CommonDatabaseOperatorType,
  isCommonDatabaseOperatorObject,
} from "./CommonDatabase";
import { DatabaseConditionBuilder } from "./DatabaseConditionBuilder";
import { DatabaseConditionParser } from "./DatabaseConditionParser";
import { DatabaseOperator } from "./DatabaseOperator";
import { isRegExpObject } from "./Helper/isRegExpObject";
import { LogicalDatabaseOperator } from "./LogicalDatabaseOperator";

/**
 * Parses given conditions and create according operators.
 */
export abstract class AbstractDatabaseConditionParser<TModel = {}>
  implements DatabaseConditionParser<TModel> {
  public parse<TValue = unknown>(
    condition: CommonDatabaseOperatorObject
  ): DatabaseOperator<TValue> | LogicalDatabaseOperator<TModel> | null {
    let value;
    if (Array.isArray(condition.value)) {
      // check if an array of values is passed
      value = condition.value.map((singleValue) =>
        typeof singleValue === "object" &&
        isCommonDatabaseOperatorObject(singleValue)
          ? this.parse(singleValue)
          : singleValue
      );
    } else if (
      typeof condition.value === "object" &&
      isCommonDatabaseOperatorObject(condition.value)
    ) {
      value = this.parse(condition.value);
    } else if (condition.type === CommonDatabaseOperatorType.REGEXP) {
      if (typeof condition.value === "string") {
        value = new RegExp(condition.value);
      } else if (isRegExpObject(condition.value)) {
        value = new RegExp(condition.value.pattern, condition.value.flags);
      }
    } else if (
      typeof condition.value === "string" &&
      this.isDate(condition.value)
    ) {
      value = this.parseDate(condition.value);
    } else {
      value = condition.value;
    }

    if (isCommonAtomDatabaseOperatorObject(condition)) {
      return this.getConditionBuilder().createAtomOperatorByType<TValue>(
        condition.type,
        condition.key,
        value
      );
    }
    if (isCommonSearchDatabaseOperatorObject(condition)) {
      return this.getConditionBuilder().createSearchOperatorByType(
        condition.type,
        value,
        condition.options
      );
    }

    return this.getConditionBuilder().createLogicalOperatorByType(
      condition.type,
      value
    );
  }

  /**
   * Checks if the given string is a date format.
   *
   * @param value The value to check.
   */
  protected abstract isDate(value: string): boolean;

  /**
   * Parses the given string to a date object.
   *
   * @param dateString The string to parse.
   */
  protected parseDate(dateString: string): Date {
    return new Date(dateString);
  }

  /**
   * Return the condition builder for this parser.
   */
  protected abstract getConditionBuilder(): DatabaseConditionBuilder<TModel>;
}
