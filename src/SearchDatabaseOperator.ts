import { DatabaseOperator } from "./DatabaseOperator";
import { SearchOptions } from "./SearchOptions";

/**
 * Represents a search operator which performs a database seArch against the value of this operator.
 */
export interface SearchDatabaseOperator<TValue extends string = string>
  extends DatabaseOperator<TValue> {
  /**
   * Returns the options for this database operator.
   */
  getOptions(): SearchOptions;
}
