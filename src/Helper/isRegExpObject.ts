/**
 * Interface for an reg exp object representation.
 */
export interface RegExpObject {
  /**
   * The reg exp pattern.
   */
  pattern: string;
  /**
   * Flags for the reg exp.
   */
  flags?: string;
}

/**
 * Checks if the given object is a reg exp object.
 *
 * @param obj The object to check.
 */
export function isRegExpObject(obj: object): obj is RegExpObject {
  return obj && "pattern" in obj;
}
