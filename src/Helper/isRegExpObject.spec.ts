import { isRegExpObject } from "./isRegExpObject";

describe("isRegExpObject", () => {
  it("should identify the object correctly", async () => {
    const regExp = /foo/i;
    const regExpObject = {
      flags: regExp.flags,
      pattern: regExp.source,
    };
    expect(isRegExpObject(regExpObject)).toEqual(true);
    const regExpObjectWithoutFlags = {
      pattern: regExp.source,
    };
    expect(isRegExpObject(regExpObjectWithoutFlags)).toEqual(true);
    const regExpObjectWithoutPattern = {
      flags: regExp.flags,
    };
    expect(isRegExpObject(regExpObjectWithoutPattern)).toEqual(false);
    const emptyObject = {};
    expect(isRegExpObject(emptyObject)).toEqual(false);
  });
});
