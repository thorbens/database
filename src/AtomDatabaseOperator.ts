import { DatabaseOperator } from "./DatabaseOperator";
import { KeyModel } from "./KeyModel";

/**
 * Represents a condition for a query where the key should match the given value.
 * To transform this condition to a suitable representation, use the toObject method.
 *
 * @typeparam TModel The model used for this operator.
 * @typeparam TValue The value used for this operator.
 */
export interface AtomDatabaseOperator<TModel = {}, TValue = unknown>
  extends DatabaseOperator<TValue> {
  /**
   * Returns the key for this condition.
   */
  getKey(): KeyModel<TModel>;
}
