import { DatabaseOperator } from "./DatabaseOperator";
import { DatabaseOrderByDirection } from "./DatabaseOrderByDirection";
import { DatabaseQuery } from "./DatabaseQuery";
import { KeyModel } from "./KeyModel";
import { AggregationFields } from "./ReadOnlyDatabaseQuery";

/**
 * Abstract Database query implementation which provides basic functionality for a database query.
 */
export class AbstractDatabaseQuery<TSource, TResult = TSource>
  implements DatabaseQuery<TSource, TResult> {
  /**
   * The limit for this query.
   */
  private limit = 0;
  /**
   * The offset for this query.
   */
  private offset = 0;
  /**
   * The order by key for this query.
   */
  private orderBy: string | null = null;
  /**
   * The order by direction for this query.
   */
  private orderByDirection = DatabaseOrderByDirection.ASC;
  /**
   * The conditions set for this query.
   */
  private readonly conditions: DatabaseOperator[] = [];
  /**
   * The list of fields to be returned.
   */
  private fields: ReadonlyArray<KeyModel<TResult>> = [];
  /**
   * The list of fields to be aggregated.
   */
  private aggregationFields: AggregationFields<TSource> = {};

  public addCondition(condition: DatabaseOperator): void {
    if (!Object.isFrozen(condition)) {
      Object.freeze(condition);
    }
    this.conditions.push(condition);
  }

  public clearConditions(): void {
    this.conditions.length = 0;
  }

  public hasConditions(): boolean {
    return this.conditions.length > 0;
  }

  public getConditions(): DatabaseOperator[] {
    return this.conditions.slice();
  }

  public getLimit(): number {
    return this.limit;
  }

  public setLimit(limit: number): void {
    this.limit = limit;
  }

  public getOffset(): number {
    return this.offset;
  }

  public setOffset(offset: number): void {
    this.offset = offset;
  }

  public getOrderBy(): string | null {
    return this.orderBy;
  }

  public setOrderBy(orderBy: string | null): void {
    this.orderBy = orderBy;
  }

  public getOrderByDirection(): DatabaseOrderByDirection {
    return this.orderByDirection;
  }

  public setOrderByDirection(orderByDirection: DatabaseOrderByDirection): void {
    this.orderByDirection = orderByDirection;
  }

  public getFields(): ReadonlyArray<KeyModel<TResult>> {
    return this.fields;
  }

  public setFields(fields: ReadonlyArray<KeyModel<TResult>>): void {
    this.fields = fields;
  }

  public toObject(): object {
    // build object
    return this.conditions.reduce(
      (obj, condition) => ({
        ...obj,
        ...condition.toObject(),
      }),
      {}
    );
  }

  public setAggregationFields(fields: AggregationFields<TSource>): void {
    this.aggregationFields = fields;
  }

  public getAggregationFields(): AggregationFields<TSource> {
    return this.aggregationFields;
  }
}
