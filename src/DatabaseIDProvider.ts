/**
 * Ensures that this object has an id.
 */
export interface DatabaseIDProvider extends Object {
  /**
   * The id for the database element.
   */
  readonly id: string;
}
