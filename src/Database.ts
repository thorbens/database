import { DatabaseConditionBuilder } from "./DatabaseConditionBuilder";
import { DatabaseConditionParser } from "./DatabaseConditionParser";
import { DatabaseModel } from "./DatabaseModel";
import { DatabaseQuery } from "./DatabaseQuery";
import { Repository } from "./Repository";

export type RepositoryConstructor<T extends DatabaseModel> = new (
  ...args: unknown[]
) => T;

/**
 * Abstract class for a database.
 */
export abstract class Database {
  /**
   * Opens a connection to the database.
   */
  public abstract connect(): Promise<void>;

  /**
   * If a connection is established, it will be disconnected.
   */
  public abstract disconnect(): Promise<void>;

  /**
   * Returns the repository for the given type.
   *
   * @typeparam T The model type for the repository to retrieve.
   * @param type The repository to retrieve.
   */
  public abstract getRepository<T extends DatabaseModel>(
    type: RepositoryConstructor<T>
  ): Promise<Repository<T>>;

  /**
   * Deletes the repository for the given type.
   * This method should be preferred over the {@link Repository#drop} method,
   * as the database may cache the repository and/or perform mapping related operations
   * related to the repository in {@link getRepository}.
   *
   * @typeparam T The model type for the repository to delete.
   * @param type The repository to delete.
   */
  public abstract dropRepository<T extends DatabaseModel>(
    type: RepositoryConstructor<T>
  ): Promise<void>;

  /**
   * Creates a new query with the given model if typed.
   *
   * @typeparam T The model used for the query.
   */
  public abstract createQuery<T extends DatabaseModel>(): DatabaseQuery<T>;

  /**
   * Returns a condition builder used to fill a DatabaseQuery.
   */
  public abstract getConditionBuilder<
    TModel = {}
  >(): DatabaseConditionBuilder<TModel>;

  /**
   * Returns a condition parser used to parse condition from an object.
   */
  public abstract getConditionParser<
    TModel = {}
  >(): DatabaseConditionParser<TModel>;
}
