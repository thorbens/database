import { AbstractDatabaseOperator } from "./AbstractDatabaseOperator";
import { CommonSearchDatabaseOperatorObject } from "./CommonDatabase";
import { SearchDatabaseOperator } from "./SearchDatabaseOperator";
import { SearchOptions } from "./SearchOptions";

/**
 * Abstract implementation of a search operator.
 */
export abstract class AbstractSearchDatabaseOperator<
    TValue extends string = string
  >
  extends AbstractDatabaseOperator<TValue>
  implements SearchDatabaseOperator<TValue> {
  private readonly searchOptions: SearchOptions;

  public constructor(searchTerm: TValue, options?: SearchOptions) {
    super(searchTerm);
    this.searchOptions = options || {};
  }

  public toCommonObject(): CommonSearchDatabaseOperatorObject {
    return {
      ...super.toCommonObject(),
      options: this.getOptions(),
    };
  }

  public getOptions(): SearchOptions {
    return this.searchOptions;
  }
}
