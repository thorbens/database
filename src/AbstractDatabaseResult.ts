import { DatabaseAggregationMap, DatabaseResult } from "./DatabaseResult";
import { ReadOnlyDatabaseQuery } from "./ReadOnlyDatabaseQuery";

/**
 * Represents a database result.
 *
 * @typeparam T The model used for this result.
 */
export abstract class AbstractDatabaseResult<TSource, TResult = TSource>
  implements DatabaseResult<TSource, TResult> {
  private readonly query: ReadOnlyDatabaseQuery<TSource, TResult>;
  private readonly results: TResult[];
  private readonly size: number;

  protected constructor(
    query: ReadOnlyDatabaseQuery<TSource, TResult>,
    results: TResult[]
  ) {
    this.query = query;
    this.results = results;
    this.size = Array.isArray(results) ? results.length : 1;
  }

  public getQuery(): ReadOnlyDatabaseQuery<TSource, TResult> {
    return this.query;
  }

  public getResults(): TResult[] {
    return this.results;
  }

  public getFirstResult(): TResult | null {
    return this.results[0] ?? null;
  }

  public getSize(): number {
    return this.size;
  }

  public abstract getTotalSize(): Promise<number>;

  public abstract getAggregations(): Promise<DatabaseAggregationMap>;
}
